
#include "ProgramInputHandler.h"
#include "SpaceMetricGrid.h"
#include "global_shader_path.h"


int main()
{
    ProgramInputHandler::renderer_light_textured.SetGlobalScale({6 / 8.f, 1, 1});
    return ProgramInputHandler::RunProgram(new SpaceMetricGrid(60.f), shader_path, 800, 600);
}
