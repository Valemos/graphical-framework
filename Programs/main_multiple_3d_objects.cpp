
#include "programs_source/ProgramMultipleObjects.h"
#include "ProgramInputHandler.h"
#include "global_shader_path.h"

int main()
{
	return ProgramInputHandler::RunProgram(new ProgramMultipleObjects(60.f), shader_path, 800, 600);;
}
