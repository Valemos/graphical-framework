
#include "ProgramInputHandler.h"
#include "GridVisualization.h"
#include "global_shader_path.h"


int main()
{
    const float grid_resolution = 2.f / 20; // from -1 to 1 there are N cells
    const int intermediate_points = 1;
    const int grid_points_x = 100;
    const int grid_points_y = 100;

    auto* program = new GridVisualization(
            { -grid_points_x * grid_resolution / 2, -grid_points_x * grid_resolution / 2 },
            { grid_resolution, grid_resolution },
            grid_points_x, grid_points_y,
            intermediate_points
    );

    return ProgramInputHandler::RunProgram(program, shader_path, 700, 700);
}